
# example/example02 - 여러 버전의 Java 환경 구축 및 컨테이너 띄워보기
- docker 저장소의 이미지를 기반으로 하여 커스텀 이미지를 만들어(Dockerfile) 빌드해본다.
  - [openjdk - Docker Hub](https://hub.docker.com/_/openjdk)
- 빌드한 이미지를 컨테이너에서 실행하여 결과를 출력한다.

## cd hello-docker/example/example02
```bash
# wsl2 또는 vagrant ssh에서
cd ~/hello-docker/example/example02  # or your path
```

## [Main.java](./Main.java)
```java
public class Main {
  public static void main(String[] args) {
    System.out.println(String.format("Hello! Java %s", System.getProperty("java.version")));
    // System.out.println("Hello! Java " + System.getProperty("java.version"));
  }
}
```

## Dockerfile
### [Dockerfile](./Dockerfile)
### [jdk8.Dockerfile](./jdk8.Dockerfile)
### [jdk9.Dockerfile](./jdk9.Dockerfile)

## Java 1.7
- [Dockerfile (./Dockerfile)](./Dockerfile)
- ```bash
  # ./Dockerfile (jdk7) 빌드
  docker build -t my_jdk_7 .
  # docker build -t my_jdk_7 -f Dockerfile .
  docker run --rm --name my_java_7_container my_jdk_7

  # Hello! Java 1.7.xxx
  ```

## Java 1.8
- [Dockerfile (./jdk8.Dockerfile)](./jdk8.Dockerfile)
- ```bash
  # ./jdk8.Dockerfile (jdk8) 빌드
  docker build -t my_jdk_8 -f jdk8.Dockerfile .
  docker run --rm --name my_java_8_container my_jdk_8

  # Hello! Java 1.8.xxx
  ```

## Java 9
- [Dockerfile (./jdk9.Dockerfile)](./jdk9.Dockerfile)
- ```bash
  # ./jdk9.Dockerfile (jdk9) 빌드
  docker build -t my_jdk_9 -f jdk9.Dockerfile .
  docker run --rm --name my_java_9_container my_jdk_9

  # Hello! Java 9.xxx
  ```

---
[메뉴](../../README.md)
