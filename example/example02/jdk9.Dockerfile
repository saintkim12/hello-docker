FROM openjdk:9
COPY ./Main.java /usr/src/myapp/Main.java
WORKDIR /usr/src/myapp
RUN javac Main.java
CMD ["java", "Main"]
