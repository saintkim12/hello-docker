# example/example03 - docker-compose로 서비스 실행해보기
- docker-compose로 서비스를 실행하기 위해, docker-compose.yml 파일을 작성한다.
- docker-compose로 서비스를 실행하고, docker와 비교해본다.

## [docker-compose.yml](./docker-compose.yml)
```yml
version: '2'
services:
  hello-http:
    image: strm/helloworld-http
```

## docker-compose
```bash
# cd ~/hello-docker/example/example03
docker-compose up -d --build
```

## docker와 비교하며 확인해보기
```bash
docker-compose ps
# docker ps -a

docker-compose logs -f hello-http
# docker logs -f hello-http => X

# docker-compose down
docker-compose rm -fs hello-http
# docker rm -f hello-http
```

---
[메뉴](../../README.md)
