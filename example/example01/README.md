# example/example01 - Tomcat 서버 띄워보기
[Tomcat 이미지](https://hub.docker.com/_/tomcat)를 사용하여 docker 서비스를 실행해본다.

## wsl 또는 vagrant 띄우기
```bash
# wsl2이라면, cmd 창에서
wsl
```
```bash
# vagrant라면, cmd 창에서
cd %USERPROFILE%
# git-bash라면 cd ~
cd ubuntu_20.04
vagrant up
vagrant ssh
```

## ip, port 관련 작업
- wsl2라면 바로 localhost를 쓰면 된다
- vagrant라면 Vagrantfile을 수정하여 reload한다
```ruby
# Vagrantfile: 
# ...
  config.vm.network "forwarded_port", guest: 8080, host: 8080, host_ip: "127.0.0.1"
  config.vm.network "forwarded_port", guest: 8081, host: 8081, host_ip: "127.0.0.1"
  config.vm.network "forwarded_port", guest: 8082, host: 8082, host_ip: "127.0.0.1"
  config.vm.network "forwarded_port", guest: 8083, host: 8083, host_ip: "127.0.0.1"
  config.vm.network "forwarded_port", guest: 13306, host: 13306, host_ip: "127.0.0.1"
# ...
```
```bash
# in cmd
cd %USERPROFILE%/ubuntu_20.04
# vi Vagrantfile
# 또는 메모장에서 수정
vagrant reload
vagrant ssh
```

## docker
```bash
# tomcat 저장소의 9.0.27-jdk8 버전의 이미지를 받아와서 실행
# https://hub.docker.com/_/tomcat
# in cmd
docker run --name tomcat --rm -dit -p 8080:8080 tomcat:9.0.27-jdk8
docker ps -a
# http://localhost:8080
# 컨테이너 중단
docker rm -f tomcat
```

## http://localhost:8080
![tomcat page](img/2021-05-13-11-17-06.png)

---
[메뉴](../../README.md)
