# example/example04 - docker-compose로 여러 서비스 관리하기
- 여러 서비스를 docker-compose로 관리해본다.
## 서비스 목록
host ip: localhost(127.0.0.1) 기준
| 서비스명 | 포트/주소/URL | 설명 |
| :-- | :-- | :-- |
| tomcat | http://localhost:8080 | 샘플 war 파일이 배포된 톰캣 |
| mariadb | localhost:13306 | MariaDB |
| mongodb | X (:27017) | MongoDB |
| mongo-express | http://localhost:8081 | MongoDB web Interface |
| web01 | http://localhost:8082 | 테스트용 웹서버 |
| web02 | http://localhost:8083 | 테스트용 웹서버 |

## [docker-compose.yml](./docker-compose.yml)
필요시 확인

## docker-compose 명령어 실행
```bash
# cd ~/hello-docker/example/example04
docker-compose up -d --build
docker-compose ps
```
![docker-compose ps](img/2021-05-13-16-45-13.png)

## docker-compose로 컨테이너 관리하기
## 특정 서비스 종료
```bash
docker-compose rm -fs web01 tomcat
docker-compose ps
```
## 특정 서비스만 실행
```bash
docker-compose up -d --build tomcat
docker-compose ps
```
## 서비스 로그 조회
```bash
docker-compose logs -f mongo-express mongodb
# Ctrl + C로 종료
```
## 전체 서비스 종료 및 정리
```bash
docker-compose down
docker-compose ps
```

---
[메뉴](../../README.md)
