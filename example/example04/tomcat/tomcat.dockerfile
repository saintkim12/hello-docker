FROM tomcat:9.0.27-jdk8
ARG WAR_FILE="ROOT.war"
# 톰캣 기본 페이지(ROOT) 폴더 삭제
RUN rm -rf webapps/ROOT
COPY $WAR_FILE webapps/ROOT.war
