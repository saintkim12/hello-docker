# Hello Docker

## 목록
0. [교육 전 필요 작업](./docs/00_pre-work.md)
1. [Docker](./docs/01_docker.md)
  - [example01 - Tomcat 서버 띄워보기](./example/example01/README.md)
  - [example02 - 여러 버전의 Java 환경 구축 및 컨테이너 띄워보기](./example/example02/README.md)
2. [Docker Compose](./docs/02_docker-compose.md)
  - [example03 - docker-compose로 서비스 실행해보기](./example/example03/README.md)
  - [example04 - docker-compose로 여러 서비스 관리하기](./example/example04/README.md)


## git clone
저장소를 clone받아 세팅된 환경에서 진행할 수 있다.
```bash
# wsl(wsl2) or vagrant ssh
cd ~
# if in windows
# cd %USERPROFILE%
git clone https://gitlab.com/saintkim12/hello-docker.git
```
