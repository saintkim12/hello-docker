# docker-compose

## Why docker-compose?
- Docker Compose는 다중 컨테이너 애플리케이션을 정의하고 공유 할 수 있도록 개발 된 도구
- yaml 파일을 사용하여 서비스 정의
### Only Docker
- 복잡해지는 docker run 명령어
  ```bash
    # mariadb: https://hub.docker.com/_/mariadb
    docker run --name some-mariadb -v /my/custom:/etc/mysql/conf.d -e MYSQL_ROOT_PASSWORD=my-secret-pw -p 3306:3306 -d mariadb:latest
    ## <>
    docker run \
      --name some-mariadb \
      -v /my/custom:/etc/mysql/conf.d \
      -e MYSQL_ROOT_PASSWORD=my-secret-pw \
      -p 3306:3306 \
      -d \
      mariadb:latest
  ```
- 시스템 내에서 작동하는 컨테이너의 관리가 힘듬
  - 어느 경로에서든 docker run 사용 가능
  - 네트워크, 볼륨 사용 시, docker network|volume 명령어로 추가적인 관리가 필요할 수 있음

### With docker-compose
- 길어지는 docker run 및 명령어를 yml 파일로 관리 가능
  - Before
    ```bash
      # CLI
      docker run \
        --name some-mariadb \
        -v /my/custom:/etc/mysql/conf.d \
        -e MYSQL_ROOT_PASSWORD=my-secret-pw \
        -p 3306:3306 \
        -d \
        mariadb:latest
    ```
  - After
    ```yaml
      # docker-compose.yml
      # ...
      services:
        some-mariadb:
          container_name: some-mariadb
          image: mariadb:latest
          volumes:
            - /my/custom:/etc/mysql/conf.d
          environments:
            -  MYSQL_ROOT_PASSWORD=my-secret-pw
          ports:
            - 3306:3306
      # ...
    ```
    ```bash
      docker-compose up -d --build some-mariadb
    ```
- docker-compose.yml 파일별로 컨테이너들이 묶임
  - 묶인 컨테이너 간 동일 네트워크를 사용하도록 자동 설정하므로 서로간 통신 가능
  - 컨테이너를 일괄적으로 관리 가능(동시 시작, 동시 종료 등)
  - docker-compose ps 명령어로 docker-compose.yml이 관리하는 컨테이너들의 상태를 조회 가능
- 일부 docker 설정을 위해 필요한 명령을 자동으로 수행
  - network
  - volume
  - image (build)
  - ...
- docker 및 컨테이너를 쉽게 사용/관리하기 위한 유틸/확장프로그램으로 생각하면 되며, docker-compose를 설치하더라도 docker 명령어를 사용하는데는 지장이 없음
- 다른 docker 컨테이너에 영향이 없도록 그룹 내의 자원만 관리

### 단점/부작용
- docker-compose 사용을 위해서는 추가 설치가 필요
- 폴더별(docker-compose.yml 파일별)로 컨테이너들(그룹)이 분리됨
  - 같은 그룹에 있지 않은 docker 서비스와 통신을 위해서는 네트워크 추가 설정이 필요
  - (CLI 기준) docker-compose.yml이 있는 폴더에서만 명령어가 작동하므로 인자를 통해 docker-compose.yml을 명시적으로 지정하거나 .sh 파일로 정리해야 한다
    ```bash
      # case 1
      cd /path/to/compose
      docker-compose up
      # case 2
      # in anywhere
      docker-compose -f /path/to/compose/docker-compose.yml up
    ```
- docker-compose 명령어 확인 필요

## docker-compose.yml
- docker-compose 서비스 관련 설정사항이 들어있는 yml 파일

## 주요 명령어(docker-compose)
```bash
  # 특정/전체 컨테이너 빌드 및 실행
  docker-compose up -d --build [서비스명]

  # 모든 컨테이너 및 생성된 네트워크, 볼륨 삭제
  docker-compose down

  # 특정/전체 컨테이너 삭제
  docker-compose rm -fs [서비스명]

  # 특정/전체 컨테이너 로그 조회(follow)
  docker-compose logs -f [서비스명]

  # 특정 컨테이너에 명령어 실행(쉘 접근 등)
  docker-compose exec 서비스명 명령어
  # docker-compose exec mariadb bash
```

# 실습
- [example03 - docker-compose로 서비스 실행해보기](../example/example03/README.md)
  - helloworld-http 이미지를 사용한 컨테이너를 docker-compose로 실행
- [example04 - docker-compose로 여러 서비스 관리하기](../example/example04/README.md)
  - 여러 서비스를 docker-compose로 관리

---
[메뉴](../README.md)
