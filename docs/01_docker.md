# Docker?
## Why VM?
### VM(Virtual Machine)
- 호스트 OS 내에서, 호스트 OS와 분리된 OS(게스트 OS)를 가상화하여 사용
- 한 OS 내에서 다른 OS 사용 가능
- 한 OS 내의 자원을 내부의 게스트 OS로 분리하여 사용
### 개발 환경 측면에서의 VM
#### 한 OS를 사용하는 경우
- 기존에는 다양한 프로그램들을 한 OS에 설치하여야 했음
  - OS마다 설정방법이 달라지게 된다.
  - 프로그램 간 충돌(동일 포트 사용 등)
  - 삭제 후 잔여 파일/설정이 존재할 수 있음
  - 다양한 운영 환경에 맞춘 프로그램을 세팅해야 하는 경우 복잡해짐
    (어쩌면 필요에 따라 프로그램을 재설치해야하거나, 환경마다 컴퓨터(+초기세팅)가 필요할 수도 있음)
    - Java 버전
    - DB 버전
    - ...
#### VM을 사용하여 OS를 분리
- 각 프로그램이 세팅된 VM을 개발 PC에 띄울 수 있음
- 개발 환경별 VM을 세팅하여, 각 작업 대상에 맞는 환경을 구축할 수 있음

### 참고
- [가상 머신(VM)이란?](https://www.redhat.com/ko/topics/virtualization/what-is-a-virtual-machine)

## Why Docker?
- VM과 비슷한 개념인 컨테이너를 사용
- VM은 게스트 OS 및 프로그램을 구동하기 위한 자원(CPU, Memory 등)을 할당 및 고정적으로 사용해야 함.  
  **Docker 컨테이너는 프로세스에 필요한만큼만 자원을 사용**
- VM이 (OS 가상화를 통한) OS간의 분리 방식으로 실행했다면, Docker는 프로세스들을 컨테이너 내에 격리시키는 방식으로 실행
- 작성한 dockerfile로 이미지를 빌드하거나, 저장소(docker hub)에 올라간 이미지로 컨테이너를 만드므로, 동일한 환경(컨테이너)을 쉽게 재구축 가능
  - VM의 이미지화(OS 백업)는 OS 및 설치된 프로그램 등을 그대로 이미지화하므로 용량이 큼
  - docker의 이미지화는 명령어의 집합인 Dockerfile로 구현되므로 필요한 데이터를 작게 유지할 수 있음
- 필요한 이미지들을 저장소 형태로 제공 및 공유
  - https://hub.docker.com/
- Docker 명령어로 컨테이너 관리 가능

### 참고
- [Docker의 이해](https://cloud.kt.com/portal/user-guide/education-eduadvanced-edu_adv_2)

---

# Docker 사용해보기

## 웹서버 실행시켜보기: strm/helloworld-http
### Docker Hub에서 이미지 조회 및 사용
1. https://hub.docker.com/r/strm/helloworld-http
2. 문서 및 사용법 확인
3. docker 서비스 실행시키기
```bash
  # docker run 명령어 사용
  ## --rm: 컨테이너가 존재하면 자동적으로 삭제하고 실행(재생성)
  ## -i: Interactive 모드로 표준입력과 표준출력을 키보드와 화면을 통해 가능하도록 하는 옵션이다.
  ## -t: 텍스트 기반의 터미널(TTY)을 애뮬레이션해주는 옵션이다.
  ### -it 형태로 같이 쓰이는데, docker container 내부에서 shell 명령어를 통해 관리할 때 실시간으로 전달되도록 함
  ### https://www.daleseo.com/docker-run/
  ## -p: 포트 매핑
  ### (host의 80 포트를):(내부 컨테이너의 80포트에) 매핑시킨다
  ## strm/helloworld-http: 사용할 이미지명
  docker run --rm -it -p 80:80 strm/helloworld-http
```
4. 작동사항 확인
- 웹브라우저에서 http://localhost (http://localhost:80) 실행하여 확인
  - 만약 Vagrant 등 VM이라면 VM의 ip 확인 및 port가 열려있는지 확인해야 함
  - vagrant라면 Vagrantfile을 수정하여 vagrant reload한다.
  - ![heloworld-http](img/2021-05-11-15-13-52.png)
- 또는 명령어로 확인
  - ```bash
      # 기존 cmd 창은 container를 잡고 있으므로, 새 cmd 창에서 진행해야 함
      # curl으로 host에서 http://localhost 의 결과물을 확인
      curl localhost
    ```
  - ![curl](img/2021-05-13-11-04-10.png)
5. Ctrl+C (환경에 따라 다를 수 있음)를 cmd 창에 입력하여 작동중인 서버 종료하기
  - 웹페이지가 작동하지 않는 걸 확인하기
6. -d 옵션을 주어 백그라운드로 실행시키기
```bash
  # -d: 백그라운드에서 컨테이너 실행
  docker run --rm -itd -p 80:80 strm/helloworld-http
  # 해쉬값(컨테이너 ID) 출력과 함께 입력 가능한 것을 확인할 수 있음
```
7. 컨테이너 상태 조회
```bash
  # docker ps: 컨테이너 상태 조회
  # -a: 모든 컨테이너 조회, -a 옵션이 없으면 실행중인 컨테이너만 조회됨
  docker ps -a
```

# 주요 개념 정리
## image
- docker hub 또는 Dockerfile을 통해 정의된, 컨테이너를 구성하기 위한 템플릿
- image를 그대로 사용하거나, 기본 image 위에 중첩하는 방식으로 사용할 수 있다. (FROM 이미지)
## container
- image를 기반으로 생성(빌드)된 process가 작동하는 격리된 공간
## volume
- container에서 생성하고 사용하는 데이터를 유지하기 위한 기본 메커니즘
- volume으로 연결하지 않은 데이터는 container 삭제 시 제거되나, container 상태에 상관없이 유지(보존)되어야 하는 데이터(Database 등)는 volume으로 연결하여 host에 보존시켜 관리할 수 있다.
## network
- 물리적인 네트워크와 비슷한 docker 내 가상의 네트워크 개념
- container 간, 또는 host와 container를 연결해야할 때 동일한 (가상의) 네트워크에 두어 각각 통신을 할 수 있게 한다.

# 주요 명령어 정리(Docker)
## docker / docker --help
모든 명령어 조회 가능

## docker run 이미지명
이미지를 컨테이너에 실행시킨다.

### 옵션
- \-d: 컨테이너를 백그라운드에서 실행
- \-\-name \[컨테이너명\]: 컨테이너의 이름을 지정(미지정시 컨테이너 관리를 위해 컨테이너 아이디를 확인해야 함)
- \-\-network \[네트워크명\]: (생성된 가상의) docker 네트워크에 컨테이너를 연결시킨다.
- \-p: 컨테이너가 제공하는(EXPOSE) 포트를 host에 연결시킨다.(host 포트:container 포트)
- \-e: 환경 변수 전달
- \-v: 컨테이너와 host의 볼륨(데이터, 파일시스템)을 연결한다. host의 파일을 컨테이너 내부에서 사용하도록 연결하거나, 컨테이너에서 작성된 파일을 host에서 관리하기 위해 사용한다.(host 경로:container 경로)

### 참고사항
- 이렇게 docker run 명령어가 길어지거나, 컨테이너를 위해 세팅해야하는 명령이 많아진다.
- 이를 yaml 파일을 통해 컨테이너 등 docker 서비스를 실행/관리하기 위한 툴이 Docker Compose.

## docker ps
실행중인 모든 컨테이너들을 조회한다.

### 옵션
- \-a: (정지된 컨테이너 포함)모든 컨테이너를 조회한다.

## docker build PATH
- Dockerfile로 docker 이미지를 빌드한다.
- 저장소에 올라간 이미지는 build가 필요없으나, 커스텀 Dockerfile을 사용하는 이미지는 반드시 빌드를 먼저 해줘야 함

## docker cp SRC DEST
컨테이너와 host 간 파일을 복사한다.

## docker exec 컨테이너명 명령어
컨테이너에 명령어를 전달한다.

## docker logs 컨테이너명
컨테이너의 로그(STDOUT, STDERR)를 조회한다.

---

# Dockerfile
- docker image를 생성하기 위한 명령어가 정의된 text파일
- docker는 image 생성(빌드) 시 Dockerfile을 읽어 이미지를 생성한다.
- Dockerfile 문법으로 작성되어 있음
## Dockerfile 주요 문법
명령 인자 인자...
### FROM 이미지명
기본이 되는 이미지 로드
### ENV
- 환경 변수값 지정
- ENV 변수명으로 선언했으면, $변수명 또는 ${변수명}으로 사용 가능
### ARG
- Dockerfile 내에서 사용할 인자값(**빌드시에만 사용**)
- docker build --build-arg 에서 값을 지정할 수 있으며, 없을 시 Dockerfile에 정의한 기본값 사용
  ```bash
    # Dockerfile
    ## ...
    ARG user=default_user
    USER $user # 내부
    ## ...
  ```
  ```bash
    # cmd
    docker build .                              # user = default_user
    docker build --build-args user=ext_user .   # user = ext_user
  ```

### COPY|ADD src dest
host의 파일을 컨테이너 내부 경로에 복사(추가)
### RUN, CMD, ENTRYPOINT 명령어/[명령어...]
- 컨테이너 내부에서 명령어를 수행
- 참고: [[Docker] RUN vs CMD vs ENTRYPOINT in Dockerfile](https://blog.leocat.kr/notes/2017/01/08/docker-run-vs-cmd-vs-entrypoint)
### EXPOSE
- 컨테이너 내부의 포트를 host 및 다른 네트워크 내의 Docker 서비스가 사용할 수 있도록 공개

# 실습
- [example01 - Tomcat 서버 띄워보기](../example/example01/README.md)
  - Tomcat 이미지를 사용하여 docker 서비스 실행 및 확인
- [example02 - 여러 버전의 Java 환경 구축 및 컨테이너 띄워보기](../example/example02/README.md)
  - Java 버전(7,8,9,...)별로 이미지 빌드(Dockerfile)
  - 빌드한 이미지로 컨테이너 생성하고 확인

---
[메뉴](../README.md)
