# 교육 전 필요 작업
windows 10 기준 작성

![로드맵](img/2021-05-11-14-21-33.png)

## git for windows 설치
https://git-scm.com/downloads
- git 서버에 미리 작업된 저장소를 받아오기 위함
- 설치 시 선택하는 게 많은데 그냥 기본 값 선택

## CLI(Command Line Interface) Tools 준비
- [CLI Tools 준비](./00-01_cli.md)
- 설정하지 않고 cmd/git-bash를 사용해도 무방함

## 리눅스 환경 준비
- 다음 중 **하나를 선택**하여 리눅스 환경을 설정한다.
  - WSL2와 다른 VM을 같이 설정하면 올바르게 세팅이 안될 수 있음
- 환경 준비 후 docker를 설치한다(아래)

---

### 개발 PC에 리눅스 설치
링크를 따라가서 원하는 리눅스 환경을 설정한다.
#### WSL2
[WSL2 설치하기](./00-02_wsl2.md)
#### VM(VirtualBox)
[Vagrant + VirtualBox 설치하기](./00-03_vagrant.md)
#### 기타 VM
- 사용하는/선호하는 VM이 있으면 자유롭게 사용 가능
- 호스트(Windows 10)에서 확인해야 하는 경우 네트워크 및 포트만 올바르게 해주면 문제 없을 듯
### 리눅스가 설치된 컴퓨터 준비
개인 PC에서 원격으로 접속할 수 있도록 환경을 갖추어도 OK

---

## 리눅스에 Docker 설치하기
```bash
  # wsl2를 설치한 경우: cmd에서
  wsl
  # vagrant를 설치한 경우: cmd에서
  vagrant ssh
  # 기타 vm을 설치한 경우: vm 내부에서
  #
```
```bash
  # docker 설치하기: https://docs.docker.com/engine/install/ubuntu/
  # update
  sudo apt-get remove docker docker-engine docker.io containerd runc
  sudo apt-get update
  # install docker
  curl -fsSL https://get.docker.com -o get-docker.sh
  sudo sh get-docker.sh
  # root 권한 없이 docker를 실행하도록 현 사용자를 docker 그룹에 추가
  sudo usermod -aG docker $USER
  ## docker 서비스를 실행 및 활성화
  # sudo service docker start
  # sudo systemctl enable docker

  # docker compose 설치하기: https://docs.docker.com/compose/install/
  sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  # exit
```
```bash
  # in ssh
  # docker 작동 확인을 위해 테스트 이미지 빌드
  docker run hello-world
  # docker daemon이 실행중이 아닐 경우, 다음 명령어를 수행해볼 것
  # sudo service docker start
  # sudo systemctl enable docker

  ###
  # vagrant@vagrant:~$ docker run hello-world
  # Unable to find image 'hello-world:latest' locally
  # latest: Pulling from library/hello-world
  # b8dfde127a29: Pull complete
  # Digest: sha256:f2266cbfc127c960fd30e76b7c792dc23b588c0db76233517e1891a4e357d519
  # Status: Downloaded newer image for hello-world:latest
  # 
  # Hello from Docker!
  # This message shows that your installation appears to be working correctly.
  # 
  # To generate this message, Docker took the following steps:
  # 1. The Docker client contacted the Docker daemon.
  # 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
  #     (amd64)
  # 3. The Docker daemon created a new container from that image which runs the
  #     executable that produces the output you are currently reading.
  # 4. The Docker daemon streamed that output to the Docker client, which sent it
  #     to your terminal.
  # 
  # To try something more ambitious, you can run an Ubuntu container with:
  # $ docker run -it ubuntu bash
  # 
  # Share images, automate workflows, and more with a free Docker ID:
  # https://hub.docker.com/
  # 
  # For more examples and ideas, visit:
  # https://docs.docker.com/get-started/
  ###
```

## Q. Docker Desktop(Windows용 Docker)을 사용하면 되지 않나요?
- Docker Desktop에서 Linux 컨테이너 또는 Windows 컨테이너를 실행하는 것은 가능하나, 두 유형의 컨테이너를 같이 사용하는 것은 문제가 있다고 함
- WSL2 설치 후, Docker Desktop을 WSL2에 연결시키면 리눅스 커널 기반 이미지도 정상적으로 사용 가능
  - WSL2 설치/활성화 후 Docker Desktop을 WSL2에 연결하면 Docker Desktop에서도 WSL2의 docker 제어 가능

### Q. 어떤걸 설치해야할 지 모르겠어요
- ~~개발 PC가 가상화(Hyper-V)를 지원하고,~~ Windows 버전이 최신 또는 특정 버전 이상인 경우 => **WSL2**
  - x64 시스템의 경우: 버전 1903 이상, 빌드 18362 이상
  - ARM64 시스템의 경우: 버전 2004 이상, 빌드 19041 이상
  - [출처: Windows 10에 Linux용 Windows 하위 시스템 설치 가이드](https://docs.microsoft.com/ko-kr/windows/wsl/install-win10)
- ~~개발 PC가 가상화(Hyper-V)를 지원하고,~~ Windows 버전 업데이트가 가능하고, 업데이트 후 기타 개발 환경에 이상이 없는 경우 => 업데이트 후 **WSL2**
- Windows 10이 아니거나, ~~가상화(Hyper-V)가 불가능하거나,~~ 여러 이유로 Windows 버전업이 불가능한 경우 => Vagrant + VirtualBox
  - 이미 사용하는 VirtualBox가 있는 등 VirtualBox 사용이 불가능한 경우 => Other VMs 또는 알아서

- P.S. Windows Server에서는 WSL2 사용이 불가능한 것으로 보임
  - https://github.com/MicrosoftDocs/WSL/issues/678

## 참고
참고용이므로 진행 안해도 됩니다.
### vagrant ip 조회
```bash
  # vagrant ssh -c "hostname -I | cut -d' ' -f2" 2>/dev/null
  vagrant ssh -c "hostname -I | cut -d' ' -f2"
  # 172.17.0.1
  # Connection to 127.0.0.1 closed.
```

### vagrant port-forwarding
https://m.blog.naver.com/sory1008/220755013450

### Hyper-V 활성화
- WSL2 사용할 때, 필요시 Hyper-V 활성화(WSL 성능 업)
  - [Windows 10에 Hyper-V 설치](https://docs.microsoft.com/ko-kr/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)
  - [Hyper-V 활성화- Google 검색](https://www.google.com/search?q=hyper-v+%ED%99%9C%EC%84%B1%ED%99%94&newwindow=1&ei=jwybYNGzGZLj0ATysJ_4Cg&oq=hyper-v+%ED%99%9C%EC%84%B1%ED%99%94&gs_lcp=Cgdnd3Mtd2l6EAMyAggAMgYIABAIEB4yBggAEAgQHjIGCAAQCBAeMgYIABAIEB4yBggAEAgQHjoFCAAQsAM6CQgAELADEAgQHjoECAAQDToICAAQCBAHEB5QsyNYmCRguSVoAXAAeACAAXuIAbECkgEDMi4xmAEAoAEBqgEHZ3dzLXdpesgBBsABAQ&sclient=gws-wiz&ved=0ahUKEwiRt53j3cLwAhWSMZQKHXLYB68Q4dUDCA4&uact=5)
- [활성화하지 않아도 WSL2가 작동한다고 함](https://github.com/MicrosoftDocs/WSL/issues/899#issuecomment-690753034)
> 주의: CPU 등 컴퓨터 자원에 따라 활성화가 불가능할 수 있음

### Docker Desktop 설치
- WSL2 사용 시 windows GUI로 docker를 관리하기 위해 사용할 수 있음
- [Docker Desktop](https://www.docker.com/products/docker-desktop)
- 설치 중 또는 후에 설정에서 Use the WSL 2 based engine를 활성화하기
#### Tutorial 진행해보기
- 끝까지 진행해보기
- ![Docker Desktop Tutorial](img/2021-05-11-13-30-14.png)

---
[메뉴](../README.md)
