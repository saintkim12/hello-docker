
# Vagrant (+ VirtualBox)
VM 환경을 쉽게 갖출 수 있는 Vagrant 툴 사용
## VirtualBox 및 Vagrant 설치하기
- VirtualBox 5.2 버전 설치
  - https://www.virtualbox.org/wiki/Download_Old_Builds_5_2
  - 최신버전(6.1)은 Vagrant 연결 시 문제가 있다하여 옛 버전인 5.2 사용
  - ![Vagrant 5.2.44-Windows hosts](img/2021-05-13-10-41-54.png)
- Vagrant 최신버전 설치
  - https://www.vagrantup.com/downloads
- 설치 후 재시작

## Vagrant로 Ubuntu 20.04 설치하기
```bash
  # 권한 문제때문에 사용자 기본폴더 사용, 아니면 cmd를 관리자 권한으로 실행하거나 권한있는 임의의 폴더 사용
  cd %USERPROFILE%
  # git-bash$ cd ~
  mkdir ubuntu_20.04
  cd ubuntu_20.04
  vagrant init bento/ubuntu-20.04
  vagrant up
  # 설치 후
  vagrant ssh
```
```bash
  # in ssh
  sudo passwd root
  # New password:
  su -
  exit
  # 이후 Docker 설치하기
```

---
[뒤로](./00_pre-work.md)
